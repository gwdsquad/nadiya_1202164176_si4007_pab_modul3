package com.example.android.modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {
    private ArrayList<User> dattarUser;
    private Context mContext;

    public AdapterUser (ArrayList<User> dattarUser, Context mContext){
        this.dattarUser = dattarUser;
        this.mContext = mContext;
    }

    public AdapterUser.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_main, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterUser.ViewHolder viewHolder, int urutan) {
        User currentUser = dattarUser.get(urutan);
        viewHolder.bindTo(currentUser);
    }

    public int getItemCount(){ return dattarUser.size();}

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView nama, pekerjaan;
        private ImageView foto;
        private int avatarCode;

        public ViewHolder(View itemView){
        super(itemView);

        nama = itemView.findViewById(R.id.tx_nama);
        pekerjaan = itemView.findViewById(R.id.tx_pekerjaan);
        foto = itemView.findViewById(R.id.avatar);

        itemView.setOnClickListener(this);}

        void bindTo(User currentUser){
            nama.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1:
                    foto.setImageResource(R.drawable.ic_man);
                    break;
                case 2:
                    foto.setImageResource(R.drawable.ic_woman);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(), DetailActivity.class);
            toDetailActivity.putExtra("nama", nama.getText().toString());
            toDetailActivity.putExtra("gender", avatarCode);
            toDetailActivity.putExtra("pekerjaan", pekerjaan.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }
}


