package com.example.android.modul3;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    private TextView DetailNama, DetailPekerjaan;
    private ImageView DetailFoto;
    private int avatarCode;
    private String mNama, mPekerjaan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        DetailNama = findViewById(R.id.tx_detail_Nama);
        DetailPekerjaan = findViewById(R.id.tx_detail_Pekerjaan);
        DetailFoto = findViewById(R.id.ig_detail_Avatar);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");;
        avatarCode = getIntent().getIntExtra("gender", 2);

        DetailNama.setText(mNama);
        DetailPekerjaan.setText(mPekerjaan);
        switch (avatarCode){
            case 1:
                DetailFoto.setImageResource(R.drawable.ic_man);
                break;
            case 2:
                DetailFoto.setImageResource(R.drawable.ic_woman);
                break;
        }
    }
}
